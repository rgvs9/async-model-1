
import da
PatternExpr_168 = da.pat.TuplePattern([da.pat.ConstantPattern('Ping')])
PatternExpr_173 = da.pat.FreePattern('p')
_config_object = {}

class Coordinator(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_0', PatternExpr_168, sources=[PatternExpr_173], destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_167])])

    def setup(self):
        pass

    def _da_run_internal(self):
        super()._label('_st_label_163', block=False)
        _st_label_163 = 0
        while (_st_label_163 == 0):
            _st_label_163 += 1
            if 0:
                _st_label_163 += 1
            else:
                super()._label('_st_label_163', block=True)
                _st_label_163 -= 1

    def _Coordinator_handler_167(self, p):
        self.output('Pinged')
        self._send(('Pong',), p)
    _Coordinator_handler_167._labels = None
    _Coordinator_handler_167._notlabels = None
