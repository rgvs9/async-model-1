# sample code for reading a policy.  CSE 535, Fall 2016, Scott Stoller.

import xml.etree.ElementTree as ET

def main():
    tree = ET.parse('policy-example.xml')
    root = tree.getroot()
    set_of_rules = []
    for rule in root.iter('rule'):
        this_rule = {}
        this_rule['name'] = rule.attrib['name']
        sc=rule.find('subjectCondition')
        this_rule['sc'] = sc.attrib
        rc=rule.find('resourceCondition')
        this_rule['rc'] = rc.attrib
        act=rule.find('action')
        this_rule['act'] = act.attrib
        su=rule.find('subjectUpdate')
        if su != None:
            this_rule['su'] = su.attrib
        ru=rule.find('resourceUpdate')
        if ru != None:
            this_rule['ru'] = ru.attrib
        print(this_rule)
        set_of_rules.append(this_rule)
    print (set_of_rules)
        
main()
