************************************************************
Distributed History-Based Access Control Policy Evaluation
************************************************************

**-----------**
INTRODUCTION
**-----------**

* We have implemented a distributed coordinator based policy evaluation scheme which scales well and prevent incorrect decisions
due to concurrency.(Listed in paper "Scalable and Secure Concurrent Evaluation of History-based Access Control Policies" 
by Maarten Decat, Bert Lagaisse, Wouter JooseniMinds-DistriNet, KU Leuven, 3001 Leuven, Belgium)
* We have used DistAgo 1.0.0b17 for our implementation.

**---------------**
MODULES &  DESIGN
**---------------**

* We have created five processes Master, Client, Coordinator, Worker and Database. The description of them is as follows:
* We also have a config file which states various parameters needed by the different processes.

1.) Config.txt :
----------------
* The config file contains the following:
1.) Number_of_clients
2.) Number_of_coordinators
3.) Number_of_workers_per_coordinator
4.) Policy_file_name
5.) DB_init_file
6.) MindDB_latency
7.) MaxdDB_latency
8.)The workload of each client is specified in the following format:
<Client_number, Subject_id, Resource_id, Action>
where Subject_id and Resource_id are the unique identifiers identifying the subject and resource in question.


2.) MASTER :
-----------
* The master reads from the config file the number of workers,coordinators,clients and other parameters listed up.
* It starts all other processes.

3.) Client:
-----------
* The client reads the config file, reads the requests that are associated with it(Based on a client number)  and starts a loop for those requests.
* It waits until it receives a response for one request and then it sends another request.
* The number of clients started are mentioned in the config file.
* It finds the coordinator by a hash function which finds the value based on the subject_id and the number of coordinators.

4.) Coordinator:
----------------
	a.) Subject Coordinator:
		* First the request reaches a subject coordinator.
		* The subject coordinator gives a global unique id called "GID" to the policy request.
		* It maintains a queue "LOS" for ongoing evaluations at subject and to check the ordering conflicts.
		* It has a tentative attribute list called S_R_Cache to have tentative updates.
		* It adds subj_attr_list(tentative update) to the policy.
		* It has a DB_cache, in which it flushes values before commiting them to the database.
		* It also maintains a Dep_dict for all the dependencies. In this, it adds, for every policy all the other policies it is 
		  dependent on.
		* It adds the attributes in the tentative attribute list and passes the new policy to the resource coordinator.
		* It receives the policy evaluation result from the worker and resource conflict result from resource coordinator, and accordingly
		  passes the result to the client.

	b.) Resource Coordinator:
		* 

		
5.)Worker:
------------
* Worker receives request from the resource coordinator to evaluate the policy and also has cached subject and resource attribute lists.
* On the recieve of this request, worker reads the value from the database, if values are not passed from the coordinator.
* It evaluates the policy. All the conditions for subject and resource have been parsed by a parser function evaluate_policy.
* The result of the policy evaluation is sent back to the subject coordinator.


**--------------------------**
Instruction to Run the Code:
**--------------------------**


* There is a run.sh script which contains all the commands to compile all the source files and run the master process.
* To run the master process, type the following into the terminal:

	



 







