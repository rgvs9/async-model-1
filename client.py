
import da
PatternExpr_184 = da.pat.TuplePattern([da.pat.ConstantPattern('Pong')])
_config_object = {}

class Client(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_ClientReceivedEvent_0', PatternExpr_184, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Client_handler_183])])

    def setup(self, p, nrounds):
        self.p = p
        self.nrounds = nrounds
        pass

    def _da_run_internal(self):
        for i in range(self.nrounds):
            self._send(('Ping',), self.p)
            super()._label('_st_label_179', block=False)
            _st_label_179 = 0
            while (_st_label_179 == 0):
                _st_label_179 += 1
                if 0:
                    _st_label_179 += 1
                else:
                    super()._label('_st_label_179', block=True)
                    _st_label_179 -= 1
            else:
                if (_st_label_179 != 2):
                    continue
            if (_st_label_179 != 2):
                break

    def _Client_handler_183(self):
        self.output('Ponged.')
    _Client_handler_183._labels = None
    _Client_handler_183._notlabels = None
