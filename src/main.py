
import da
_config_object = {}
from client import Client
from coordinator import Coordinator
from itertools import islice
from database import Database
from worker import Worker
import sys

def main():
    config_file = sys.argv[1]
    with open(config_file) as f:
        lines = f.readlines()
    nocl = int(lines[0].strip('\n').split('=')[1])
    noc = int(lines[1].strip('\n').split('=')[1])
    now = int(lines[2].strip('\n').split('=')[1])
    pol_file_name = lines[3].strip('\n').split('=')[1]
    db_init_file = lines[4].strip('\n').split('=')[1]
    db = da.new(Database, (), num=1)
    worker = da.new(Worker, (db, config_file), num=now)
    coordinator = da.new(Coordinator, num=noc)
    for co_od in coordinator:
        da.setup(co_od, (coordinator, noc, worker, db))
    client = da.new(Client, num=nocl)
    cli_num = 1
    for c in client:
        da.setup(c, (coordinator, cli_num, noc, config_file))
        cli_num += 1
    da.start(worker)
    da.start(db)
    da.start(coordinator)
    da.start(client)
