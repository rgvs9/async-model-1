
import da
PatternExpr_440 = da.pat.TuplePattern([da.pat.ConstantPattern('Policy Evaluation Request'), da.pat.FreePattern('pol_eval_req'), da.pat.FreePattern('l')])
PatternExpr_449 = da.pat.FreePattern('p')
PatternExpr_588 = da.pat.TuplePattern([da.pat.ConstantPattern('Failure at worker'), da.pat.FreePattern('pol_eval_res')])
PatternExpr_612 = da.pat.TuplePattern([da.pat.ConstantPattern('Success at worker'), da.pat.FreePattern('pol_eval_res'), da.pat.FreePattern('new_subj_attr_list'), da.pat.FreePattern('new_res_attr_list'), da.pat.FreePattern('old_subj_attr_list'), da.pat.FreePattern('old_res_attr_list')])
PatternExpr_788 = da.pat.TuplePattern([da.pat.ConstantPattern('I have done conflict resolution. Now you do SC'), da.pat.FreePattern('pol_eval_res'), da.pat.FreePattern('new_subj_attr_list')])
PatternExpr_894 = da.pat.TuplePattern([da.pat.ConstantPattern('Conflict detected at RC'), da.pat.FreePattern('pol_eval_res'), da.pat.FreePattern('new_subj_attr_list')])
PatternExpr_982 = da.pat.TuplePattern([da.pat.ConstantPattern('Authorization Request'), da.pat.FreePattern('pol_eval_req'), da.pat.FreePattern('subj_attr_list')])
PatternExpr_991 = da.pat.FreePattern('p')
PatternExpr_1076 = da.pat.TuplePattern([da.pat.ConstantPattern('Worker has done his work. Now check for conflicts RC'), da.pat.FreePattern('pol_eval_res'), da.pat.FreePattern('new_res_attr_list'), da.pat.FreePattern('new_subj_attr_list'), da.pat.FreePattern('old_res_attr_list')])
_config_object = {}
import uuid
import time

class Coordinator(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_0', PatternExpr_440, sources=[PatternExpr_449], destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_439]), da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_1', PatternExpr_588, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_587]), da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_2', PatternExpr_612, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_611]), da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_3', PatternExpr_788, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_787]), da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_4', PatternExpr_894, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_893]), da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_5', PatternExpr_982, sources=[PatternExpr_991], destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_981]), da.pat.EventPattern(da.pat.ReceivedEvent, '_CoordinatorReceivedEvent_6', PatternExpr_1076, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Coordinator_handler_1075])])

    def setup(self, q, noc, w, db):
        self.q = q
        self.noc = noc
        self.w = w
        self.db = db
        self.S_R_Cache = {}
        self.DB_Cache = {}
        self.LOS = {}
        self.Dep_dict = {}
        self.flag = 0

    def _da_run_internal(self):
        super()._label('_st_label_238', block=False)
        _st_label_238 = 0
        while (_st_label_238 == 0):
            _st_label_238 += 1
            if 0:
                _st_label_238 += 1
            else:
                super()._label('_st_label_238', block=True)
                _st_label_238 -= 1

    def getAttributes(self, S_R_id):
        Attribute_list = {}
        if (S_R_id in self.DB_Cache):
            Attribute_list.update(self.DB_Cache[S_R_id])
        if (S_R_id in self.S_R_Cache):
            Attribute_list.update(self.S_R_Cache[S_R_id])
        return Attribute_list

    def restart_evaluation(self, pol_eval_res):
        Gid = pol_eval_res['Gid']
        sub_id = pol_eval_res['Sub_id']
        if (sub_id in self.LOS):
            self.LOS[sub_id].append(Gid)
        else:
            self.LOS[sub_id] = [Gid]
        self.Dep_dict[Gid] = set()
        subj_attr_list = self.getAttributes(str(pol_eval_res['Sub_id']))
        for (attr_name, value) in subj_attr_list.items():
            if (len(value) == 3):
                self.Dep_dict[Gid].add(value[2])
        res_co_id = (int(''.join((str(ord(c)) for c in pol_eval_res['Res_id']))) % self.noc)
        pol_eval_res['Sub_cood_id'] = self.id
        self.output(Gid, 'Authorization Request has been sent to Resource Coordinator')
        self._send(('Authorization Request', pol_eval_res, subj_attr_list), self.q[res_co_id])

    def check_subj_conflict(self, sub_id, old_subj_attr_list):
        if (sub_id in self.DB_Cache):
            if (len(old_subj_attr_list) == 0):
                return True
            else:
                for (key, val) in old_subj_attr_list.items():
                    if (key in self.DB_Cache[sub_id]):
                        if (self.DB_Cache[sub_id][key][1] > val[1]):
                            return True
        if (sub_id in self.S_R_Cache):
            if (len(old_subj_attr_list) == 0):
                return True
            else:
                for (key, val) in old_subj_attr_list.items():
                    if (key in self.S_R_Cache[sub_id]):
                        if (self.S_R_Cache[sub_id][key][1] > val[1]):
                            return True
        return False

    def check_res_conflict(self, res_id, old_res_attr_list):
        if (res_id in self.DB_Cache):
            if (len(old_res_attr_list) == 0):
                return True
        else:
            for (key, val) in old_res_attr_list.items():
                if (key in self.DB_Cache[res_id]):
                    if (self.DB_Cache[res_id][key][1] > val[1]):
                        return True
        return False

    def _Coordinator_handler_439(self, pol_eval_req, l, p):
        self.q = list(self.q)
        Gid = ((('C' + pol_eval_req['Client']) + 'R') + str(int((l + 1))))
        pol_eval_req['Gid'] = Gid
        sub_id = pol_eval_req['Sub_id']
        if (sub_id in self.LOS):
            self.LOS[sub_id].append(Gid)
        else:
            self.LOS[sub_id] = [Gid]
        self.Dep_dict[Gid] = [set(), pol_eval_req]
        subj_attr_list = self.getAttributes(str(pol_eval_req['Sub_id']))
        for (attr_name, value) in subj_attr_list.items():
            if (len(value) == 3):
                self.Dep_dict[Gid][0].add(value[2])
        res_co_id = (int(''.join((str(ord(c)) for c in pol_eval_req['Res_id']))) % self.noc)
        pol_eval_req['Sub_cood_id'] = self.id
        self.output(Gid, 'Authorization Request has been sent to Resource Coordinator')
        self._send(('Authorization Request', pol_eval_req, subj_attr_list), self.q[res_co_id])
    _Coordinator_handler_439._labels = None
    _Coordinator_handler_439._notlabels = None

    def _Coordinator_handler_587(self, pol_eval_res):
        self.output(pol_eval_res['Gid'], 'Failure received from worker for G-ID', pol_eval_res['Gid'])
        self._send(('Request Failed', pol_eval_res), pol_eval_res['Client_id'])
    _Coordinator_handler_587._labels = None
    _Coordinator_handler_587._notlabels = None

    def _Coordinator_handler_611(self, pol_eval_res, new_subj_attr_list, new_res_attr_list, old_subj_attr_list, old_res_attr_list):
        sub_id = pol_eval_res['Sub_id']
        res_id = pol_eval_res['Res_id']
        Gid = pol_eval_res['Gid']
        self.output(pol_eval_res['Gid'], 'Success received from worker.')
        self.output(pol_eval_res['Gid'], 'Waiting for LOS', self.LOS[sub_id][0])
        super()._label('_st_label_660', block=False)
        _st_label_660 = 0
        while (_st_label_660 == 0):
            _st_label_660 += 1
            if (self.flag == 0):
                _st_label_660 += 1
            else:
                super()._label('_st_label_660', block=True)
                _st_label_660 -= 1
        self.flag = 1
        self.output(pol_eval_res['Gid'], 'waiting for Dependencyto be empty', len(self.Dep_dict[Gid][0]))
        super()._label('_st_label_679', block=False)
        _st_label_679 = 0
        while (_st_label_679 == 0):
            _st_label_679 += 1
            if (len(self.Dep_dict[Gid][0]) == 0):
                _st_label_679 += 1
            else:
                super()._label('_st_label_679', block=True)
                _st_label_679 -= 1
        self.output(pol_eval_res['Gid'], 'checking for Conflicts at Subject Coordinator')
        if (self.check_subj_conflict(sub_id, old_subj_attr_list) == False):
            if (not (len(new_subj_attr_list) == 0)):
                if (not (sub_id in self.S_R_Cache)):
                    self.S_R_Cache[sub_id] = {}
                for key in new_subj_attr_list:
                    self.S_R_Cache[sub_id][key] = [new_subj_attr_list[key], (1000 * time.time()), Gid]
            self.Dep_dict.pop(Gid)
            self.flag = 0
            self.output(pol_eval_res['Gid'], 'No Conflicts are Detected. Added tentative updates.\nCurrent Tentative Updates List:', self.S_R_Cache)
            self.output(pol_eval_res['Gid'], 'Sent to Resource Coordinator for Conflict Detection')
            self._send(('Worker has done his work. Now check for conflicts RC', pol_eval_res, new_res_attr_list, new_subj_attr_list, old_res_attr_list), pol_eval_res['Res_cood_id'])
        else:
            self.output(pol_eval_res['Gid'], 'Conflict Detected for Gid', Gid, 'Clearing Administration and Restarting Evalution')
            self.flag = 0
            self.Dep_dict.pop(Gid)
            self.restart_evaluation(pol_eval_res)
    _Coordinator_handler_611._labels = None
    _Coordinator_handler_611._notlabels = None

    def _Coordinator_handler_787(self, pol_eval_res, new_subj_attr_list):
        self.output(pol_eval_res['Gid'], 'conflict resolution completed by RC received at SC')
        sub_id = pol_eval_res['Sub_id']
        Gid = pol_eval_res['Gid']
        if (not (len(new_subj_attr_list) == 0)):
            if (not (sub_id in self.DB_Cache)):
                self.DB_Cache[sub_id] = {}
            for key in new_subj_attr_list:
                self.DB_Cache[sub_id][key] = [new_subj_attr_list[key], (1000 * time.time())]
            self.output('Commit to database sent from SC to Database')
            self._send(('commit to database', sub_id, new_subj_attr_list), self.db)
        for (key, value) in self.Dep_dict.items():
            self.Dep_dict[key][0].discard(Gid)
        if (sub_id in self.S_R_Cache):
            self.S_R_Cache.pop(sub_id)
        self._send(('The result is this:', pol_eval_res), pol_eval_res['Client_id'])
    _Coordinator_handler_787._labels = None
    _Coordinator_handler_787._notlabels = None

    def _Coordinator_handler_893(self, pol_eval_res, new_subj_attr_list):
        list_of_policies_dependent = []
        self.output(pol_eval_res['Gid'], 'Message recieved at SC that Conflict Detected at RC and Clearing Administration and removing the updated attributed value ')
        for (Gid, dependency_list) in self.Dep_dict.items():
            if (pol_eval_res['Gid'] in dependency_list):
                list_of_policies_dependent.append(self.Dep_dict.pop(Gid)[1])
                self.LOS[pol_eval_res['Sub_id']].remove(Gid)
            if (pol_eval_res['Sub_id'] in self.S_R_Cache):
                self.S_R_Cache.pop(pol_eval_res['Sub_id'])
            self.output(pol_eval_res['Gid'], 'Restarting Evalution of Request where Gid', Gid, 'and all other Request that are dependent on it.')
            self.restart_evaluation(pol_eval_res)
            for policy in list_of_policies_dependent:
                self.restart_evaluation(policy)
    _Coordinator_handler_893._labels = None
    _Coordinator_handler_893._notlabels = None

    def _Coordinator_handler_981(self, pol_eval_req, subj_attr_list, p):
        self.output(pol_eval_req['Gid'], 'Received Authorization Request at Resource Coordinator')
        pol_eval_req['Res_cood_id'] = self.id
        res_attr_list = self.getAttributes(str(pol_eval_req['Res_id']))
        self.output(pol_eval_req['Gid'], 'Sent to Worker for Evaluation')
        self._send(('Please Evaluate Policy for this request', pol_eval_req, subj_attr_list, res_attr_list), self.w)
    _Coordinator_handler_981._labels = None
    _Coordinator_handler_981._notlabels = None

    def _Coordinator_handler_1075(self, pol_eval_res, new_res_attr_list, new_subj_attr_list, old_res_attr_list):
        self.output(pol_eval_res['Gid'], 'Received conflict resolution request at RC and ckecking for Conflicts')
        res_id = pol_eval_res['Res_id']
        if (self.check_res_conflict(res_id, old_res_attr_list) == True):
            self.output(pol_eval_res['Gid'], 'Conflict Detected at RC')
            print(pol_eval_res['Sub_cood_id'])
            self._send(('Conflict detected at RC', pol_eval_res, new_subj_attr_list), pol_eval_res['Sub_cood_id'])
        else:
            if (not (len(new_res_attr_list) == 0)):
                if (not (res_id in self.DB_Cache)):
                    self.DB_Cache[res_id] = {}
                for key in new_res_attr_list:
                    self.DB_Cache[res_id][key] = [new_res_attr_list[key], (1000 * time.time())]
                self._send(('commit to database', res_id, new_res_attr_list), self.db)
            self.output(pol_eval_res['Gid'], 'No Conflicts Detected at RC and sent success to SC')
            self.output('Db _cache', self.DB_Cache)
            self._send(('I have done conflict resolution. Now you do SC', pol_eval_res, new_subj_attr_list), pol_eval_res['Sub_cood_id'])
    _Coordinator_handler_1075._labels = None
    _Coordinator_handler_1075._notlabels = None
