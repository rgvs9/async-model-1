
import da
PatternExpr_1201 = da.pat.TuplePattern([da.pat.ConstantPattern('Please Evaluate Policy for this request'), da.pat.FreePattern('pol_eval_req'), da.pat.FreePattern('subj_attr_list'), da.pat.FreePattern('res_attr_list')])
PatternExpr_1230 = da.pat.TuplePattern([da.pat.ConstantPattern('The values of Subject and Resource attributes are:'), da.pat.FreePattern('pol_eval_req'), da.pat.FreePattern('sub_rec'), da.pat.FreePattern('res_rec'), da.pat.FreePattern('subj_attr_list'), da.pat.FreePattern('res_attr_list')])
_config_object = {}
import xml.etree.ElementTree as ET
import json
import hashlib
import copy

class Worker(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_WorkerReceivedEvent_0', PatternExpr_1201, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Worker_handler_1200]), da.pat.EventPattern(da.pat.ReceivedEvent, '_WorkerReceivedEvent_1', PatternExpr_1230, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Worker_handler_1229])])

    def setup(self, q, config_file):
        self.q = q
        self.config_file = config_file
        with open(self.config_file) as f:
            lines = f.readlines()
        policy_file_name = lines[3].strip('\n').split('=')[1]
        tree = ET.parse(policy_file_name)
        root = tree.getroot()
        self.list_of_policy = []
        for rule in root.iter('rule'):
            policy = {}
            rule_name = rule.attrib['name']
            policy['rule'] = rule_name
            sc = rule.find('subjectCondition')
            policy['sc'] = sc.attrib
            rc = rule.find('resourceCondition')
            policy['rc'] = rc.attrib
            act = rule.find('action')
            policy['act'] = act.attrib
            su = rule.find('subjectUpdate')
            if (not (su == None)):
                policy['su'] = su.attrib
            ru = rule.find('resourceUpdate')
            if (not (ru == None)):
                policy['ru'] = ru.attrib
            self.list_of_policy.append(policy)

    def _da_run_internal(self):
        super()._label('_st_label_1196', block=False)
        _st_label_1196 = 0
        while (_st_label_1196 == 0):
            _st_label_1196 += 1
            if 0:
                _st_label_1196 += 1
            else:
                super()._label('_st_label_1196', block=True)
                _st_label_1196 -= 1

    def comp_rec(self, condition, record):
        match = True
        for (key, value) in condition.items():
            if (not (key in record)):
                match = False
                break
            elif (value[1] == 'eq'):
                if (not (value[0] == record[key])):
                    match = False
                    break
                else:
                    continue
            elif ((not value[0].isnumeric()) or (not record[key].isnumeric())):
                match = False
                break
            if (value[1] == '>'):
                match = (value[0] < record[key])
            else:
                match = (value[0] > record[key])
            if (not match):
                break
        return match

    def evaluate_policy(self, pol_eval_req, sub_rec, res_rec, action, subj_attr_list, res_attr_list):
        flag = 0
        self.output(pol_eval_req['Gid'], 'Evaluating Policy at worker where', '\nsubject record received from database is ', sub_rec, '\nresource record received from database is ', res_rec, '\ntentative and cached subject attributes from coordinator ', subj_attr_list, '\nresource attributes from coordinator ', res_attr_list)
        for key in subj_attr_list:
            if (key in sub_rec):
                sub_rec[key] = subj_attr_list[key][0]
        for key in res_attr_list:
            if (key in res_rec):
                res_rec[key] = res_attr_list[key][0]
        for policy in self.list_of_policy:
            rule = {}
            sc = {}
            rc = {}
            act = {}
            su = {}
            ru = {}
            rule = copy.deepcopy(policy['rule'])
            sc = copy.deepcopy(policy['sc'])
            rc = copy.deepcopy(policy['rc'])
            act = copy.deepcopy(policy['act'])
            if ('su' in policy):
                su = copy.deepcopy(policy['su'])
            if ('ru' in policy):
                ru = copy.deepcopy(policy['ru'])
            for (name, value) in sc.items():
                if (str(value)[0] == '>'):
                    sc[name] = [str(value)[1:], 'gt']
                elif (str(value)[0:1] == '<'):
                    sc[name] = [str(value)[1:], 'lt']
                else:
                    sc[name] = [str(value)[:], 'eq']
                if (sc[name][0][0:8] == '$resource'):
                    sc[name][0] = res_rec[sc[name][0][10:]]
                elif (sc[name][0][0:8] == '$subject'):
                    sc[name][0] = sub_rec[sc[name][0][10:]]
            for (name, value) in rc.items():
                if (str(value)[0] == '>'):
                    rc[name] = [str(value)[1:], 'gt']
                elif (str(value)[0] == '<'):
                    rc[name] = [str(value)[1:], 'lt']
                else:
                    rc[name] = [str(value)[:], 'eq']
                if (rc[name][0][0:8] == '$resource'):
                    rc[name][0] = res_rec[rc[name][0][10:]]
                elif (rc[name][0][0:8] == '$subject'):
                    rc[name][0] = sub_rec[rc[name][0][10:]]
            old_sub_attr_list = copy.deepcopy(subj_attr_list)
            old_res_attr_list = copy.deepcopy(res_attr_list)
            if (self.comp_rec(sc, sub_rec) and self.comp_rec(rc, res_rec) and (act['name'] == action)):
                flag = 1
                subj_attr_list = {}
                res_attr_list = {}
                for key in sc:
                    if (key in old_sub_attr_list):
                        subj_attr_list[key] = old_sub_attr_list[key]
                for key in rc:
                    if (key in old_res_attr_list):
                        res_attr_list[key] = old_res_attr_list[key]
                self.output(pol_eval_req['Gid'], 'The Policy is : ', policy)
                break
        self.output(pol_eval_req['Gid'], 'Policy Successfully Evaluated at worker result is sent to Subject Coordinator')
        if (flag == 1):
            new_subj_attr_list = {}
            new_res_attr_list = {}
            for (name, value) in su.items():
                if (str(value)[0:1] == '++'):
                    if sub_rec[name].isnumeric():
                        su[name] = str((int(sub_rec[name]) + 1))
                    else:
                        su[name] = str(sub_rec[name])
                elif (str(value)[0:1] == '--'):
                    if sub_rec[name].isnumeric():
                        su[name] = str((int(sub_rec[name]) - 1))
                    else:
                        su[name] = str(sub_rec[name])
                elif (su[name][0:8] == '$resource'):
                    su[name] = res_rec[su[name][0][10:]]
                elif (su[name][0:8] == '$subject'):
                    su[name] = sub_rec[su[name][0][10:]]
            for (name, value) in ru.items():
                print('name', name, 'value', value)
                if (value == '++'):
                    if res_rec[name].isnumeric():
                        print('name', name)
                        ru[name] = str((int(res_rec[name]) + 1))
                        print('ru ', ru[name])
                    else:
                        print('ru ', ru[name])
                        ru[name] = str(res_rec[name])
                elif (value == '--'):
                    if res_rec[name].isnumeric():
                        ru[name] = str((int(res_rec[name]) - 1))
                    else:
                        ru[name] = str(res_rec[name])
                elif (ru[name][0:8] == '$resource'):
                    ru[name] = res_rec[ru[name][0][10:]]
                elif (ru[name][0:8] == '$subject'):
                    ru[name] = sub_rec[ru[name][0][10:]]
            new_subj_attr_list = su
            new_res_attr_list = ru
            self._send(('Success at worker', pol_eval_req, new_subj_attr_list, new_res_attr_list, subj_attr_list, res_attr_list), pol_eval_req['Sub_cood_id'])
        else:
            self._send(('Failure at worker', pol_eval_req), pol_eval_req['Sub_cood_id'])

    def _Worker_handler_1200(self, pol_eval_req, subj_attr_list, res_attr_list):
        self.output(pol_eval_req['Gid'], 'Received', pol_eval_req['Gid'], ' Policy for evaluation at Worker.')
        self._send(('need attributes', pol_eval_req, subj_attr_list, res_attr_list), self.q)
    _Worker_handler_1200._labels = None
    _Worker_handler_1200._notlabels = None

    def _Worker_handler_1229(self, pol_eval_req, sub_rec, res_rec, subj_attr_list, res_attr_list):
        action = pol_eval_req['Action']
        self.evaluate_policy(pol_eval_req, sub_rec, res_rec, action, subj_attr_list, res_attr_list)
    _Worker_handler_1229._labels = None
    _Worker_handler_1229._notlabels = None
