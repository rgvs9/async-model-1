
import da
PatternExpr_311 = da.pat.TuplePattern([da.pat.ConstantPattern('Processing Request')])
PatternExpr_320 = da.pat.TuplePattern([da.pat.ConstantPattern('Request Failed'), da.pat.FreePattern('pol_eval_res')])
PatternExpr_338 = da.pat.TuplePattern([da.pat.ConstantPattern('The result is this:'), da.pat.FreePattern('pol_eval_res')])
_config_object = {}
from itertools import islice
from random import randint

class Client(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_ClientReceivedEvent_0', PatternExpr_311, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Client_handler_310]), da.pat.EventPattern(da.pat.ReceivedEvent, '_ClientReceivedEvent_1', PatternExpr_320, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Client_handler_319]), da.pat.EventPattern(da.pat.ReceivedEvent, '_ClientReceivedEvent_2', PatternExpr_338, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Client_handler_337])])

    def setup(self, p, cli_num, Tot_num_cd, config_file):
        self.p = p
        self.cli_num = cli_num
        self.Tot_num_cd = Tot_num_cd
        self.config_file = config_file
        self.flag = 0

    def _da_run_internal(self):
        req = self.create_req()
        self.p = list(self.p)
        l = 0
        while (l < len(req)):
            self.flag = 0
            sub_co_id = (int(req[l]['Sub_id']) % self.Tot_num_cd)
            req[l]['Client_id'] = self.id
            self._send(('Policy Evaluation Request', req[l], l), self.p[sub_co_id])
            super()._label('_st_label_299', block=False)
            _st_label_299 = 0
            while (_st_label_299 == 0):
                _st_label_299 += 1
                if (self.flag == 1):
                    _st_label_299 += 1
                else:
                    super()._label('_st_label_299', block=True)
                    _st_label_299 -= 1
            else:
                if (_st_label_299 != 2):
                    continue
            if (_st_label_299 != 2):
                break
            l = (l + 1)

    def create_req(self):
        req_list = []
        with open(self.config_file) as f:
            for line in islice(f, 8, None):
                line = line.strip('\n').split(';')
                if (int(line[0].strip('Client: ')) == self.cli_num):
                    rec_dict = dict((map(str, x.split(':')) for x in line))
                    req_list.append(rec_dict)
        return req_list

    def _Client_handler_310(self):
        self.output('OK.')
    _Client_handler_310._labels = None
    _Client_handler_310._notlabels = None

    def _Client_handler_319(self, pol_eval_res):
        self.output('Request Failed recieved at client where', 'Client Number', pol_eval_res['Client'])
        self.flag = 1
    _Client_handler_319._labels = None
    _Client_handler_319._notlabels = None

    def _Client_handler_337(self, pol_eval_res):
        self.output('Request granted at client where', 'Client Number', pol_eval_res['Client'])
        self.flag = 1
    _Client_handler_337._labels = None
    _Client_handler_337._notlabels = None
