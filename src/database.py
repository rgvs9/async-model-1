
import da
PatternExpr_380 = da.pat.TuplePattern([da.pat.ConstantPattern('commit to database'), da.pat.FreePattern('S_R_id'), da.pat.FreePattern('attr_list')])
PatternExpr_425 = da.pat.TuplePattern([da.pat.ConstantPattern('need attributes'), da.pat.FreePattern('pol_eval_req'), da.pat.FreePattern('subj_attr_list'), da.pat.FreePattern('res_attr_list')])
PatternExpr_436 = da.pat.FreePattern('w')
_config_object = {}
import xml.etree.ElementTree as ET
from random import random
import threading

class Database(da.DistProcess):

    def __init__(self, parent, initq, channel, props):
        super().__init__(parent, initq, channel, props)
        self._events.extend([da.pat.EventPattern(da.pat.ReceivedEvent, '_DatabaseReceivedEvent_0', PatternExpr_380, sources=None, destinations=None, timestamps=None, record_history=None, handlers=[self._Database_handler_379]), da.pat.EventPattern(da.pat.ReceivedEvent, '_DatabaseReceivedEvent_1', PatternExpr_425, sources=[PatternExpr_436], destinations=None, timestamps=None, record_history=None, handlers=[self._Database_handler_424])])

    def setup(self):
        self.subject_list = []
        self.resource_list = []
        with open('config_1.txt') as f:
            lines = f.readlines()
        db_init_fname = lines[4].strip('\n').split('=')[1]
        self.minimumlatency = float(lines[5].strip('\n').split('=')[1])
        self.maximumlatency = float(lines[6].strip('\n').split('=')[1])
        tree = ET.parse(db_init_fname)
        root = tree.getroot()
        for rec in root.iter('record'):
            sc = rec.find('subject')
            rc = rec.find('resource')
            if (not (sc == None)):
                self.subject_list.append(sc.attrib)
            if (not (rc == None)):
                self.resource_list.append(rc.attrib)

    def _da_run_internal(self):
        super()._label('_st_label_299', block=False)
        _st_label_299 = 0
        while (_st_label_299 == 0):
            _st_label_299 += 1
            if 0:
                _st_label_299 += 1
            else:
                super()._label('_st_label_299', block=True)
                _st_label_299 -= 1

    def commit(self, S_R_id, attr_list, r):
        for i in range(len(self.subject_list)):
            if (self.subject_list[i]['id'] == S_R_id):
                for (name, value) in attr_list.items():
                    self.subject_list[i][name] = value
        for i in range(len(self.resource_list)):
            if (self.resource_list[i]['id'] == S_R_id):
                for (name, value) in attr_list.items():
                    self.resource_list[i][name] = value
        self.output('Commited to database after latency:', r)
        print(self.subject_list, self.resource_list)

    def _Database_handler_379(self, S_R_id, attr_list):
        self.output('Received at DataBase')
        r = float(((random() * (self.maximumlatency - self.minimumlatency)) + self.minimumlatency))
        self.output('Committing to database start of latency:', r)
        t = threading.Timer(r, self.commit, args=[S_R_id, attr_list, r])
        t.start()
    _Database_handler_379._labels = None
    _Database_handler_379._notlabels = None

    def _Database_handler_424(self, pol_eval_req, subj_attr_list, res_attr_list, w):
        self.output('Message received at DB from worker requesting Attributes.')
        sub_id = pol_eval_req['Sub_id']
        res_id = pol_eval_req['Res_id']
        sub_rec = {}
        res_rec = {}
        for i in range(0, len(self.subject_list)):
            if (self.subject_list[i]['id'] == sub_id):
                sub_rec = self.subject_list[i]
        for i in range(0, len(self.resource_list)):
            if (self.resource_list[i]['id'] == res_id):
                res_rec = self.resource_list[i]
        self.output('Attributes sent to worker by DB')
        self._send(('The values of Subject and Resource attributes are:', pol_eval_req, sub_rec, res_rec, subj_attr_list, res_attr_list), w)
    _Database_handler_424._labels = None
    _Database_handler_424._notlabels = None
